package org.jacademy.sesion01.sample;

import java.util.Objects;
import java.util.logging.Logger;

import static java.lang.Double.NaN;

class SideEffectsSample {

    private final static Logger LOG = Logger.getLogger(SideEffectsSample.class.getName());

    private double accumulator;

    double averageWithSideEffects(double[] values) {
        Objects.requireNonNull(values);
        int length = values.length;
        if (length == 0) {
            throw new IllegalArgumentException("The list of values must not be empty.");
        }

        accumulator = 0;
        for (double value : values) {
            LOG.info("Current Value: " + value + " + partial result: " + accumulator);
            accumulator += value;
        }

        return accumulator / length;
    }

    double averageWithoutSideEffects(double[] values) {
        double total = 0;
        for (double value : values) {
            total += value;
        }

        int length = values.length;
        return length == 0 ? NaN : total / length;
    }

    double getAccumulator() {
        return accumulator;
    }
}
