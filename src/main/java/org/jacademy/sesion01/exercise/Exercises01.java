package org.jacademy.sesion01.exercise;

import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;

import java.time.LocalDate;
import java.util.Map;
import java.util.function.Function;

class Exercises01 {

    private Map<Product.ProductType, Double> taxes;

    Exercises01() {
    }

    /*
     * a) Creates a function that represents:
     * 3*x^2 - 6*x + 12.
     */
    Function<Double, Double> fx;

    /*
     * b) Define a function that calculates an age (difference in year between a date in the past and today).
     * Tip: Check ChronoUnit
     */
    Function<LocalDate, Long> calculateAge;

    /*
     * c.1) Define  function that calculates price plus tax for a purchase detail (product details and quantity),
     * according to the follow rules:
     * 1) Food & Medicine 0%
     * 2) Liqueur 22%
     * 3) Tobacco 19%
     * 4) ANY Other 16%
     */
    Function<PurchaseDetail, Double> calculatePriceWithTax;

    /*
     *  c.2) Re-do the previous, but using method reference instead of a lambda.
     */
    Function<PurchaseDetail, Double> calculatePriceWithTax_MethodReference;


    /*
    * d) Define a function using lambda that calculates the discount for a purchase, the discount is always 5% but only
    * for purchases greater than $1000.00.
     */
    Function<Double, Double> calculateDiscount;

}
