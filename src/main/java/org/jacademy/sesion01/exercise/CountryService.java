package org.jacademy.sesion01.exercise;

import org.jacademy.model.Country;

import java.util.Map;
import java.util.function.Supplier;

class CountryService {
    private Map<String, Country> countriesByIsoCode;

    CountryService(Supplier<Map<String, Country>> countrySupplier) {
        this.countriesByIsoCode = countrySupplier.get();
    }

    Country getCountryByIsoCode(String isoCode) {
        return countriesByIsoCode.get(isoCode);
    }
}
