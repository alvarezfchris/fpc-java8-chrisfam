package org.jacademy.sesion01.exercise;

import org.jacademy.model.Address;
import org.jacademy.model.City;

class Exercises02 {

    /*
    Tips:
    a) Locale.getISOCountries() // returns all the ISO Codes for the Countries supported by Java.
    b) new Locale("", isoCountry) // Creates a new Locale with country name.

    1.1) In the constructor create a supplier that return all the Countries supported by Java.
    1.2) Replace the supplier by a Method Reference
    */
    private CountryService countryService;

    private int counter = 1;

    /**
     * Creates a city giving a line, which contains the follow values separated by comma:
     *  - City name,
     *  - State name,
     *  - Country ISO Code
     *
     *  Example:
     *  Guadalajara, Jalisco, MX
     *  City name: Guadalajara
     *  State name: Jalisco
     *  Country name: Mexico
     *
     *  If any of the values is missing, a mock city will be returned, this city will be in the form of:
     *
     *  null
     *      *  City name: Default City #1
     *      *  State name: Default State
     *      *  Country name: Mexico
     *
     * Subsequent cities must be #2, #3, etc.
     *
     * @param cityLine
     * @return City created from the given information. Note, if any of the elements is missing or the line is null,
     * a default city will be returned
     */
    /*
    Exercise: Using Optional as holder
    2.1) Add a predicate in a filter to avoid process lines with less than 3 items.
    2.2) Add a mapper function that receives an array of Strings and creates a new City using the values of that line.
    2.3) Create a supplier function to create new Default city with a counter
    2.4) Replace all the lambdas by Method References.
    2.5) Indicate the side-effects in the result functions.
    2.5.1) Discuss the drawbacks, risks and limitations caused by those side-effects.
     */
    City generateCityFromString(String cityLine) {
        return null;
    }

    /**
     * Generates a string representing the address in the follow format:
     *
     * (line #1)
     * [line #2]
     * (city name), (state)
     * [country name]
     * Zip Code: (ZipCode)
     *
     * Output examples:
     *
     * Rafael Sanzio 150,
     * Zapopan, Jalisco
     * Zip Code: 40405
     *
     * line 2 and country name are optional, if they are missing (null or empty), the line is not included.
     * The rest of the parameters are mandatory, if missing, the entire address will be defaulted to: "Incomplete address"
     * @param address with the information to be used for the string.
     * @return address in the specified format.
     */
    /*
    Exercise 3: Using Optional as holder
    3.1) Add a lambda predicate for the filter to avoid process incomplete addresses.
    3.2) Add a lambda function mapper to convert from Address to String according to the rules.
    3.3) Replace both lambdas with Method references
     */
    String generateAddressString(Address address) {
        return null;
    }
}
