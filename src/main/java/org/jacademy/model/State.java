package org.jacademy.model;

import java.util.Objects;

public class State {
    private String name;
    private Country country;

    public State(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "State{" +
            "name='" + name + '\'' +
            ", country=" + country +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof State)) return false;
        State state = (State) o;
        return Objects.equals(name, state.name) &&
                   Objects.equals(country, state.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country);
    }
}
