package org.jacademy.model.shop;

import java.util.ArrayList;
import java.util.List;

public class Purchase {
    private List<PurchaseDetail> details = new ArrayList<>();

    public Purchase() {
    }

    public void addDetail(PurchaseDetail detail) {
        this.details.add(detail);
    }

    public List<PurchaseDetail> getDetails() {
        return details;
    }
}
