package org.jacademy.model.shop;

public class Product {

    public enum ProductType {
        Medicine, Food, Liqueur, Tobacco, Other
    }

    private String code;
    private String name;
    private double price;
    private ProductType type;

    public Product(String code, String name, double price, ProductType type) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }
}
