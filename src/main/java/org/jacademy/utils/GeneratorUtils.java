package org.jacademy.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorUtils {

    private GeneratorUtils() {
    }

    public static List<Integer> generateRandomNumbers(int amountOfNumbers, int maxNumber) {
        List<Integer> result = new ArrayList<>();

        Random generator = new Random();

        for (int i = 0; i < amountOfNumbers; i++) {
            result.add(generator.nextInt(10));
        }

        return result;
    }
}
