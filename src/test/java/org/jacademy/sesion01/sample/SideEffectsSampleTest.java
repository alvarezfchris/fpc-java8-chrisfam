package org.jacademy.sesion01.sample;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SideEffectsSampleTest {
    private static final double DELTA = 0.01;
    private static final double EXPECTED_AVG = 5.25;

    private final SideEffectsSample sideEffectsSample = new SideEffectsSample();
    private final double[] TEST_VALUES = {1.5, 3.5, 7.5, 8.5};

    @Test
    public void averageWithSideEffects() {
        System.out.println(sideEffectsSample.getAccumulator());

        assertEquals(EXPECTED_AVG, sideEffectsSample.averageWithSideEffects(TEST_VALUES), DELTA);

        System.out.println(sideEffectsSample.getAccumulator());
    }

    @Test
    public void averageWithoutSideEffects() {
        System.out.println(sideEffectsSample.getAccumulator());

        assertEquals(EXPECTED_AVG, sideEffectsSample.averageWithoutSideEffects(TEST_VALUES), DELTA);

        System.out.println(sideEffectsSample.getAccumulator());
    }

}