package org.jacademy.sesion01.sample;

import org.junit.Test;

import java.util.Optional;

public class MethodReferencesTest {

    private MethodReferences methodReference;

    @Test
    public void callMethodReferences() {
        MethodReferences methodReferences = Optional.ofNullable(methodReference)
            .orElseGet(MethodReferences::new);

        String result = Optional.of(12.45)
            .map(methodReferences::getFromInstanceMethod)
            .map(MethodReferences::getFromClassMethod)
            .map(String::toUpperCase)
            .orElse("default");

        System.out.println(result);
    }

}