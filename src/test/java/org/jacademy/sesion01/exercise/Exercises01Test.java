package org.jacademy.sesion01.exercise;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.jacademy.model.shop.Product;
import org.jacademy.model.shop.PurchaseDetail;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.jacademy.model.shop.Product.ProductType.Food;
import static org.jacademy.model.shop.Product.ProductType.Liqueur;
import static org.jacademy.model.shop.Product.ProductType.Medicine;
import static org.jacademy.model.shop.Product.ProductType.Other;
import static org.jacademy.model.shop.Product.ProductType.Tobacco;


@RunWith(DataProviderRunner.class)
public class Exercises01Test {

    private static final double DELTA_DOUBLE = 0.01;

    private final Exercises01 sut = new Exercises01();


    @Test
    @UseDataProvider("getFxTestData")
    public void verifyFx(Double x, Double expectedFx) {
        double actual = sut.fx.apply(x);

        assertThat(actual, closeTo(expectedFx, DELTA_DOUBLE));
    }

    @DataProvider
    public static Object[][] getFxTestData() {
        return new Object[][] {
            {0.0, 12.0},
            {1.0, 9.0},
            {2.0, 12.0},
            {10.0, 252.0},
            {2.275, 13.877}
        };
    }

    @Test
    @UseDataProvider("getTestDataForAge")
    public void verifyCalculateAge(LocalDate date, long expectedAge) {
        long actual = sut.calculateAge.apply(date);

        assertThat(actual, is(expectedAge));
    }

    @DataProvider
    public static Object[][] getTestDataForAge() {
        LocalDate today = LocalDate.now();
        return new Object[][] {
            {today, 0},
            {today.minusYears(17), 17},
            {today.plusYears(5), -5}
        };
    }

    @Test
    @UseDataProvider("getTestDataForProductDetailTax")
    public void verifyCalculateTaxes(double price, double quantity, Product.ProductType type, double expectedTaxAmount) {

        PurchaseDetail purchaseDetail = buildPurchaseDetail(price, quantity, type);

        double actual = sut.calculatePriceWithTax.apply(purchaseDetail);

        assertThat(actual, closeTo(expectedTaxAmount, DELTA_DOUBLE));
    }

    @Test
    @UseDataProvider("getTestDataForProductDetailTax")
    public void verifyCalculateTaxesForMethodReference(double price, double quantity, Product.ProductType type, double expectedTaxAmount) {

        PurchaseDetail purchaseDetail = buildPurchaseDetail(price, quantity, type);

        double actual = sut.calculatePriceWithTax_MethodReference.apply(purchaseDetail);

        assertThat(actual, closeTo(expectedTaxAmount, DELTA_DOUBLE));
    }

    private PurchaseDetail buildPurchaseDetail(double price, double quantity, Product.ProductType type) {
        Product product = new Product("abc", "ABC", price, type);
        return new PurchaseDetail(product, quantity);
    }

    @DataProvider
    public static Object[][] getTestDataForProductDetailTax() {
        return new Object[][] {
            {1.25, 1.0, Medicine, 1.25},
            {2.75, 7.0, Food, 19.25},
            {0.0, 1000.0, Other, 0.0},
            {12.66, 0.5, Liqueur, 7.723},
            {8.99, 17, Tobacco, 181.868},
            {17.33, 2, null, 40.21},
        };
    }

    @Test
    @UseDataProvider("getTestDataForCalculateDiscount")
    public void verifyDiscountCalulation(Double total, Double expectedDiscount) {
        double actual = sut.calculateDiscount.apply(total);

        assertThat(actual, closeTo(expectedDiscount, DELTA_DOUBLE));
    }

    @DataProvider
    public static Object[][] getTestDataForCalculateDiscount() {
        return new Object[][] {
            {0.0, 0.0},
            {1.0, 0.0},
            {1000.0, 0.0},
            {1000.01, 50.0},
            {5000.0, 250.0}
        };
    }


}