package org.jacademy.sesion01.exercise;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.jacademy.model.Address;
import org.jacademy.model.City;
import org.jacademy.model.Country;
import org.jacademy.model.State;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.Objects.nonNull;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(DataProviderRunner.class)
public class Exercises02Test {

    private static final Exercises02 sut = new Exercises02();

    @Test
    @UseDataProvider("getTestDataForCityFromString")
    public void generateCityFromString(String cityLine, City expectedCity) {
        City actual = sut.generateCityFromString(cityLine);

        assertThat(actual, is(expectedCity));
    }

    @DataProvider
    public static Object[][] getTestDataForCityFromString() {
        return new Object[][] {
            {null, buildCity("Default City #1", "Default State", "Mexico", "MX")} ,
            {"", buildCity("Default City #2", "Default State", "Mexico", "MX")} ,
            {"not, enough", buildCity("Default City #3", "Default State", "Mexico", "MX")} ,
            {"Zapopan, Jalisco, MX", buildCity("Zapopan", "Jalisco", "Mexico", "MX")},
            {"San Francisco, California, US", buildCity("San Francisco", "California", "United States", "US")},
        };
    }

    private static City buildCity(String name, String stateName, String countryName, String countryCode) {
        Country country = nonNull(countryName) && nonNull(countryCode)
            ? new Country(countryName, countryCode)
            : null;

        State state = nonNull(stateName)
            ? new State(stateName, country)
            : null;
        return new City(name, state);
    }

    @Test
    @UseDataProvider("getTestDataForAddress")
    public void generateAddressStr(Address address, String expectedAddressStr) {
        String actual = sut.generateAddressString(address);

        assertThat(actual, is(expectedAddressStr));
    }

    @DataProvider
    public static Object[][] getTestDataForAddress() {
        City completeCity = buildCity("Zapopan", "Jalisco", "Mexico", "MX");
        City incompleteCity = buildCity("Zapopan", "Jalisco", null, null);
        City incorrectCity = buildCity("Zapopan", null, null, null);

        return new Object[][]{
            {buildAddress("Rafael Sanzio 150", null, "45020", completeCity),
                "Rafael Sanzio 150\nZapopan, Jalisco\nMexico\nZip Code: 45020"},
            {buildAddress("Rafael Sanzio 150", null, "45020", incompleteCity),
                "Rafael Sanzio 150\nZapopan, Jalisco\nZip Code: 45020"},
            {buildAddress("Rafael Sanzio 150", "Camichines Vallarta", "45020", completeCity),
                "Rafael Sanzio 150\nCamichines Vallarta\nZapopan, Jalisco\nMexico\nZip Code: 45020"},
            {buildAddress("Rafael Sanzio 150", "Camichines Vallarta", "45020", incorrectCity),
                "Incomplete address"},
            {buildAddress(null, "Camichines Vallarta", "45020", incorrectCity),
                "Incomplete address"},
        };
    }

    private static Address buildAddress(
            String line1,
            String line2,
            String zipCode,
            City city) {
        return new Address(line1, line2, city, zipCode);
    }




}